import numpy as np

x = np.array([[1,2,3,4], [5,6,7,8], [9,10,11,12]])

y = np.hsplit(x, 2)
print(y)


# x = [np.array([0,1,2]), np.array([3,4,5])]
# print(x)

# y = np.array(x)
# print(y)

# x = np.array([0,1,2])
# y = np.array([3,4,5])

# z = np.concatenate((x,y), axis=0)
# print(z)


# a = np.array([[0,1], [4,5]])
# b = np.array([[2,3], [6,7]])
# c = np.array([[8,9], [12,13]])
# d = np.array([[10,11], [14,15]])

# x = np.bmat([[a, b], [c, d]])
# print(x)

# y = np.array([1,2,3,4])

# print(x@y)


# temp1 = np.concatenate((a,b), axis=1)
# temp2 = np.concatenate((c,d), axis=1)
# x = np.concatenate((temp1,temp2), axis=0)
# print(x)
# print(x@y)




